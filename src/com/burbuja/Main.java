package com.burbuja;

import javax.swing.*;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        int[] arreglo;
        int nElementos;
        int aux;

        //solicitamos de que tamaño va a ser el arreglo
        nElementos = Integer.parseInt(JOptionPane.showInputDialog("Digite la cantidad de elementos del arreglo: "));
        arreglo = new int[nElementos]; //Le asignamos el numero de elementos ingresados

        //pedimos los numeros que va a contener el arreglo
        for(int i=0; i<nElementos; i++) {
            System.out.print((i+1 + "digite un numero"));
            arreglo[i] = entrada.nextInt();
        }
        //creamos el metodo burbuja consiste en dos For anidados
        for(int i=0; i<(nElementos-1); i++) {
            for(int j=0; (j<nElementos-1); j++) {
                if(arreglo[j] > arreglo[j+1]){
                    aux = arreglo[j];
                    arreglo[j] = arreglo[j+1];
                    arreglo[j+1] = aux;
                }
            }
        }
        //mostrar el arreglo ordenado en forma creciente
        System.out.println("Arreglo ordenado de forma creciente: ");
        for(int i=0; i<nElementos; i++) {
            System.out.println(arreglo[i] );
        }
        //mostrar el arreglo ordenado de forma decreciente
        System.out.println("Arreglo ordenado de forma decreciente: ");
        for(int i=(nElementos - 1); i>=0; i--) {
            System.out.println(arreglo[i]);
        }
    }
}
